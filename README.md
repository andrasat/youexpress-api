# youexpress-api

---

## API vehicle

|Method|url|
|---|---|
|GET|https://ufgdrk9abh.execute-api.us-east-2.amazonaws.com/dev/get/vehicle/all|
|POST|https://ufgdrk9abh.execute-api.us-east-2.amazonaws.com/dev/add/vehicle|

Add Vehicle POST data
    - type : string,
    - image_url: string,
    - weight: number/integer,
    - order: number/integer
