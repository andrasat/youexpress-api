'use strict';

import {
	getAllVehicle,
	insertOneVehicle
} from './vehicle';

export {
	getAllVehicle,
	insertOneVehicle
};