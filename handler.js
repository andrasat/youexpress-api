'use strict';

import {
	getAllVehicle,
	insertOneVehicle
} from './functions';

import {
	MongoClient
} from 'mongodb';

import {
	errorJSONResponse
} from './lib/response_structure';

import config from './config/config.json';
import bluebird from 'bluebird';

global.Promise = bluebird;

var cachedDB = null;

export const getAllVehicles = async (event, ctx, callback) => {

	// https://www.mongodb.com/blog/post/optimizing-aws-lambda-performance-with-mongodb-atlas-and-nodejs
	ctx.callbackWaitsForEmptyEventLoop = false;

	try {

		const db = await connectMongoDB(config.mongodb);

		return getAllVehicle(db, callback);

	} catch (err) {
		return callback(null, errorJSONResponse(400, err));
	}
};

export const insertVehicle = async (event, ctx, callback) => {

	ctx.callbackWaitsForEmptyEventLoop = false;

	try {

		const db = await connectMongoDB(config.mongodb);
		const data = JSON.parse(event.body);

		return insertOneVehicle(db, data, callback);

	} catch (err) {
		return callback(null, errorJSONResponse(400, err));
	}
};

/**
 * [connectMongoDB]
 * Function to connect mongodb and cache it to improve lambda performance
 * @param  {string} url [mongo uri string]
 * @return {object}     [database object]
 */
async function connectMongoDB(url) {

	if (cachedDB && cachedDB.serverConfig.isConnected()) {
		return Promise.resolve(cachedDB);
	}
	const dbName = 'development';
	const client = await MongoClient.connect(url);
	const db = client.db(dbName);
	cachedDB = db;
	return cachedDB;
}