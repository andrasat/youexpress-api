'use strict';

/**
 * [successJSONResponse]
 * 
 * @param  {object} data
 * @return {object}     
 */
export const successJSONResponse = (data) => ({
	statusCode: 200,
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify(data)
});

/**
 * [errorJSONResponse]
 *
 * @param  {number} http statusCode
 * @param  {object} Error Object
 * @return {object}     
 */
export const errorJSONResponse = (statusCode, err) => {

	if (err instanceof Error === false) {
		throw new Error('This is not an Error object');
	}

	let errorData = {
		name: err.name,
		message: err.message
	};

	return {
		statusCode: +statusCode,
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(errorData)
	};
};