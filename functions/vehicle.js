'use strict';

import {
	successJSONResponse,
	errorJSONResponse
} from '../lib/response_structure';

export const getAllVehicle = async (db, cb) => {

	try {

		let cursor = db.collection('vehicle').aggregate([{
			$project: {
				type: 1,
				image_url: 1,
				weight: 1,
				order: 1,
			}
		}], {
			cursor: {
				batchSize: 100,
			},
			allowDiskUse: false,
			explain: false
		});

		let docs = await cursor.toArray();

		return cb(null, successJSONResponse(docs));

	} catch (err) {
		return cb(null, errorJSONResponse(400, err));
	}
};

export const insertOneVehicle = async (db, data, cb) => {

	try {

		const collection = await db.createCollection('vehicle', {
			validator: {
				$and: [{
					type: {
						$type: 'string'
					}
				}, {
					image_url: {
						$type: 'string'
					}
				}, {
					weight: {
						$type: 'int'
					}
				}, {
					order: {
						$type: 'int'
					}
				}]
			}
		});

		let writeOp = await collection.insertOne({
			type: data.type,
			image_url: data.image_url,
			weight: data.weight,
			order: data.order
		});

		if (writeOp.result.ok === 1) {
			return cb(null, successJSONResponse('Vehicle inserted'));
		} else {
			return cb(null, errorJSONResponse(400, writeOp));
		}

	} catch (err) {
		return cb(null, errorJSONResponse(400, err));
	}
};